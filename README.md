# Data access with JDBC

## Assignment

Write SQL scripts and build a Spring Boot application in Java. Follow the guidelines given below, feel free to expand on
the functionality. It must meet the minimum requirements prescribed.

### Instructions

1) Set up the development environment.
   1) Intellij Ultimate, Java 17, Postgres SQL driver dependency, Postgres, PgAdmin
2) Use plain Java to create a Spring Boot application through Spring Initializr, and SQL scripts to
   create a database with the following minimum requirements
   1) SQL scripts to create a database according to the specifications in Appendix A
   2) A repository implementation to access data from the provided Chinook database (see Appendix B).

### Appendix A

- Create several scripts which can be run to create a database: 
  - Setup some tables in the database 
  - Add relationships to the tables 
  - Populate the tables with data

### Appendix B

- Manipulate SQL data in Spring using a JDBC with the PostgreSQL driver. 
  - you are given a database to work with. It is called Chinook. 
  - You need to first create the database in PgAdmin, 
  - open a query tool and drag the script in and run it. 
  - all the tables and populate it.

        Chinook models the iTunes database of customers purchasing songs. You are to create a Spring Boot application, include
        the correct driver, and create a repository pattern to interact with the database.

## Usage

To use Appendix B 
  - init a database 
  - populate with tables and data from [chinook](src/main/resources/appendix-b/chinook_pg_serial_pk_proper_naming.sql)
  - fill database information in [application.properties](src/main/resources/application.properties)

### For a quick demo of Appendix A: [Create table](src/main/resources/appendix-a/01_tableCreate.sql)

### For a quick demo of Appendix B: [Customer](src/main/java/se/experis/demo/runners/CustomerRunner.java)

## Contributors

SA (@SalahAbdinoor)

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change. Please make sure to update tests as appropriate.