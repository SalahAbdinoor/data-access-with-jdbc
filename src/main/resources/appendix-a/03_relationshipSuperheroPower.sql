DROP TABLE IF EXISTS superheroes_powers;

CREATE TABLE superheroes_powers
(
    id           serial PRIMARY KEY,
    superhero_id int REFERENCES superhero (id) ON UPDATE CASCADE,
    power_id     int REFERENCES power (id) ON UPDATE CASCADE
);

select *
from superheroes_powers;
