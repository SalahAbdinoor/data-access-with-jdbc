/* superhero */

DROP TABLE IF EXISTS superhero;

CREATE TABLE superhero
(
    id     serial PRIMARY KEY,
    name   VARCHAR(50),
    alias  VARCHAR(50),
    origin VARCHAR(50)
);

/* !superhero */

/* assistant */

DROP TABLE IF EXISTS assistant;

CREATE TABLE assistant
(
    id   serial PRIMARY KEY,
    name VARCHAR(50)
);

/* !assistant */

/* power */

DROP TABLE IF EXISTS power;

CREATE TABLE power
(
    id          serial PRIMARY KEY,
    name        VARCHAR(50) UNIQUE,
    description VARCHAR(500)
);

/* !power */