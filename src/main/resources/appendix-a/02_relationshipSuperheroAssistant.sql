ALTER TABLE assistant
    ADD COLUMN superhero_id serial,
    ADD CONSTRAINT fk_assistant_superhero
        FOREIGN KEY (superhero_id)
            REFERENCES superhero (id);

SELECT *
FROM assistant;


