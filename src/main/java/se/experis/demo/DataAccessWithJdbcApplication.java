package se.experis.demo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import se.experis.demo.repository.customer.CustomerRepositoryImpl;

@SpringBootApplication
public class DataAccessWithJdbcApplication {

    public static void main(String[] args) {
        SpringApplication.run(DataAccessWithJdbcApplication.class, args);
    }
}
