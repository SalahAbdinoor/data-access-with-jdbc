package se.experis.demo.repository.customer;

import se.experis.demo.model.Customer;
import se.experis.demo.repository.CrudRepository;

import java.util.List;

public interface CustomerRepository extends CrudRepository<Customer, Integer> {
    List<Customer> findByName(String name);

    List<Customer> findByAllUsingLimitAndOffset(Integer limit, Integer offset);

    String countryWithTheMostCustomers();

    String customerWhoIsTheHighestSpender();

    List<String> mostPopularGenreForCustomer(Integer customerId);

}
