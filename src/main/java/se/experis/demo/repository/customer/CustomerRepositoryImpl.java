package se.experis.demo.repository.customer;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import se.experis.demo.model.Customer;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Repository
public class CustomerRepositoryImpl implements CustomerRepository {

    private String url;
    private String username;
    private String password;
    private List<Customer> customerList;

    public CustomerRepositoryImpl(
            @Value("${spring.datasource.url}") String url,
            @Value("${spring.datasource.username}") String username,
            @Value("${spring.datasource.password}") String password) {
        this.url = url;
        this.username = username;
        this.password = password;
    }

    @Override
    public List<Customer> findAll() {
        List<Customer> customerList = null;
        String sql = "SELECT * FROM customer";
        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            PreparedStatement preparedStatement = conn.prepareStatement(sql);

            // adds customers to list
            customerList = prepStatementCustomerList(preparedStatement);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customerList;
    }

    @Override
    public List<Customer> findById(Integer id) {
        List<Customer> customerList = null;
        String sql = "SELECT * FROM customer WHERE customer_id = ?";

        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setInt(1, id);

            // adds customers to list
            customerList = prepStatementCustomerList(preparedStatement);

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customerList;
    }

    @Override
    public List<Customer> findByName(String firstName) {
        List<Customer> customerList = null;

        String sql = "SELECT * FROM customer WHERE first_name LIKE ?";
        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            PreparedStatement preparedStatement = conn.prepareStatement(sql);

            preparedStatement.setString(1, firstName);

            // adds customers to list
            customerList = prepStatementCustomerList(preparedStatement);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customerList;
    }

    @Override
    public List<Customer> findByAllUsingLimitAndOffset(Integer limit, Integer offset) {

        String sql = "SELECT * FROM customer LIMIT ? OFFSET ?";
        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            PreparedStatement preparedStatement = conn.prepareStatement(sql);

            preparedStatement.setInt(1, limit);
            preparedStatement.setInt(2, offset);

            // adds customers to list
            customerList = prepStatementCustomerList(preparedStatement);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customerList;
    }

    @Override
    public int insert(Customer customer) {

        String sql = "INSERT INTO customer (first_name, last_name, country, postal_code, phone, email) VALUES (?,?,?,?,?,?)";
        return prepStatementPost(customer, sql);

    }

    @Override
    public int update(Customer customer) {

        String sql = "update customer set first_name = ?, last_name = ?, country = ?, postal_code=?, phone=?, email=? where customer_id = " + customer.id();

        return prepStatementPost(customer, sql);
    }

    @Override
    public String countryWithTheMostCustomers() {

        String countryWithTheMostCustomers = "";

        String sql = "SELECT country FROM customer WHERE country = ( SELECT MAX(country) FROM customer) LIMIT 1;";

        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            // adds customers to list
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                countryWithTheMostCustomers = resultSet.getString("country");
                break;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return countryWithTheMostCustomers;
    }

  @Override
    public String customerWhoIsTheHighestSpender() {

        String highestSpendingCustomer = "";

        String sql = "SELECT customer.customer_id AS customer, SUM(total) AS total, " +
                "first_name AS \"first name\", last_name AS \"last name\" " +
                "FROM invoice " +
                "INNER JOIN customer ON customer.customer_id = invoice.customer_id " +
                "GROUP by customer.customer_id " +
                "ORDER BY total " +
                "DESC limit 1 offset 0";

        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            // adds customers to list
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {

                highestSpendingCustomer = resultSet.getString("first name");

                break;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return highestSpendingCustomer;
    }

    @Override
    public List<String> mostPopularGenreForCustomer(Integer customerId) {

        List<String> genres = new ArrayList<>();

        String sql =
                "SELECT customer.customer_id AS \"customer id\", customer.first_name AS \"first name\", count(genre.name) AS \"amount of purchases\", genre.\"name\" AS genre\n" +
                "FROM customer\n" +
                "JOIN invoice ON invoice.customer_id = customer.customer_id \n" +
                "JOIN invoice_line ON invoice_line.invoice_id = invoice.invoice_id\n" +
                "JOIN track ON track.track_id = invoice_line.track_id\n" +
                "JOIN genre ON genre.genre_id = track.genre_id\n" +
                "WHERE customer.customer_id = ?\n" +
                "GROUP BY customer.customer_id, customer.first_name, genre.genre_id\n" +
                "ORDER BY COUNT( genre.\"name\") DESC\n" +
                "FETCH FIRST 1 ROW WITH TIES\n";

        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            PreparedStatement preparedStatement = conn.prepareStatement(sql);

            preparedStatement.setInt(1, customerId);
            // adds customers to list
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {

                genres.add(resultSet.getString("genre"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return genres;
    }

    @Override
    public int delete(Customer object) {
        return 0;
    }

    @Override
    public int deleteById(Integer integer) {
        return 0;
    }

    /**
     * Helper method that adds customers to list based on result from query
     *
     * @param preparedStatement sql query
     * @return list of customers that matched query
     */
    private List<Customer> prepStatementCustomerList(PreparedStatement preparedStatement) throws SQLException {

        List<Customer> customerList = new ArrayList<>();
        Customer customer;

        ResultSet resultSet = preparedStatement.executeQuery();

        while (resultSet.next()) {
            customer = new Customer(
                    resultSet.getInt("customer_id"),
                    resultSet.getString("first_name"),
                    resultSet.getString("last_name"),
                    resultSet.getString("country"),
                    resultSet.getString("postal_code"),
                    resultSet.getString("phone"),
                    resultSet.getString("email")
            );
            customerList.add(customer);
        }
        return customerList;
    }

    /**
     * Helper method that changes customer table based on query
     *
     * @param customer new/updated customer
     * @return affected rows in table
     */
    private int prepStatementPost(Customer customer, String sql) {
        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            PreparedStatement preparedStatement = conn.prepareStatement(sql);

            preparedStatement.setString(1, customer.firstName());
            preparedStatement.setString(2, customer.lastName());
            preparedStatement.setString(3, customer.country());
            preparedStatement.setString(4, customer.postalCode());
            preparedStatement.setString(5, customer.phoneNumber());
            preparedStatement.setString(6, customer.email());

            return preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

}
