package se.experis.demo.repository;

import se.experis.demo.model.Customer;

import java.util.List;

public interface CrudRepository<T, ID> {
    List<T> findAll();

    List<Customer> findById(ID id);

    int insert(T object);

    int update(T object);

    int delete(T object);

    int deleteById(ID id);
}

