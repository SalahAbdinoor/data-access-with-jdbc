package se.experis.demo.runners;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;
import se.experis.demo.model.Customer;
import se.experis.demo.repository.customer.CustomerRepository;

@Component
public class CustomerRunner implements ApplicationRunner {

    private final static Logger LOG = LoggerFactory
            .getLogger(CustomerRunner.class);

    private final CustomerRepository customerRepository;

    public CustomerRunner(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {

        // 1 find all customers
        LOG.info("Find all: " + customerRepository.findAll().toString());

        // 2 find customer by id
        LOG.info("Find by id: " + customerRepository.findById(1).toString());

        // 3 find customer by name
        LOG.info("Find by name: " + customerRepository.findByName("Daan").toString());

        // 4 find customers using limit & offset
        LOG.info("Find all using limit & offset: " + customerRepository.findByAllUsingLimitAndOffset(1, 1).toString());

        // 5 create new customer
        //LOG.info(("created new customer; rows affected were: " + customerRepository.insert(new Customer(0, "Salah", "Abdinoor", "Sweden", "165 54", "+46 70 053 63 01", "salah.abdinoor@gmail.com"))));

        // 5 update customer by id
        LOG.info(("updated customer; rows affected were: " + customerRepository.update(new Customer(60, "Test2", "Testsson", "Sweden", "165 54", "+46 70 053 63 01", "salah.abdinoor@gmail.com"))));

        // 6 country with the most customers
        LOG.info("country with the most customers: " + customerRepository.countryWithTheMostCustomers());

        // 7 customer who is the highest spender
        LOG.info("customer who is the highest spender: " + customerRepository.customerWhoIsTheHighestSpender());

        // 8 most popular genre for customer
        LOG.info("most popular genre for customer: " + customerRepository.mostPopularGenreForCustomer(12));
    }
}
